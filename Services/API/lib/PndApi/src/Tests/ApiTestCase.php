<?php
namespace PndApi\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase as BaseApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\Mapping\MappingException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\TransactionRequiredException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Doctrine\ORM\ORMException;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use InvalidArgumentException;
use LogicException;
use RuntimeException;

class ApiTestCase extends BaseApiTestCase
{
    use ReloadDatabaseTrait;

    /**
     * Get a service from the container.
     *
     * @param string $service Service name.
     * @return object|null
     *
     * @throws LogicException 
     */
    protected function get(string $service)
    {
        if (!self::$container) {
            static::bootKernel();
        }

        return self::$container->get($service);
    }

    /**
     * Get Doctrine's EntityManager.
     *
     * @return EntityManagerInterface 
     *
     * @throws LogicException 
     */
    protected function getEntityManager() : EntityManagerInterface
    {
        return $this->get('doctrine')->getManager();
    }

    /**
     * Get Doctrine Repository for the given entity class.
     *
     * @param string $className 
     * @return EntityRepository 
     * 
     * @throws LogicException 
     */
    protected function getRepository(string $className): EntityRepository
    {
        return $this->getEntityManager()->getRepository($className);
    }

    /**
     * Find a resource of the given class by ID.
     *
     * @param string $className 
     * @param mixed $id 
     * @return object|null 
     * 
     * @throws MappingException 
     * @throws OptimisticLockException 
     * @throws TransactionRequiredException 
     * @throws ORMInvalidArgumentException 
     * @throws ORMException 
     * @throws InvalidArgumentException 
     * @throws RuntimeException 
     */
    protected function findEntity(string $className, $id)
    {
        return $this->getRepository($className)->find($id);
    }

    /**
     * Get fresh instance of the given resource.
     *
     * @param mixed $entity 
     * @return object|null 
     * 
     * @throws MappingException 
     * @throws OptimisticLockException 
     * @throws TransactionRequiredException 
     * @throws ORMInvalidArgumentException 
     * @throws ORMException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     */
    protected function refreshEntity($entity)
    {
        return $this->findEntity(get_class($entity), $entity->getId());
    }

    /**
     * Persist a resource in the database.
     *
     * @param object $item 
     * @return void 
     * 
     * @throws LogicException 
     */
    protected function persist($item)
    {
        $em = $this->getEntityManager();
        $em->persist($item);
        $em->flush();
    }

    // protected function createUser(string $email, string $password): User
    // {
    //     $user = new User();
    //     $user->setEmail($email);
    //     $user->setUsername(substr($email, 0, strpos($email, '@')));

    //     $passwordEncoded = $this->get('security.password_encoder')
    //         ->encodePassword($user, $password);
    //     $user->setPassword($passwordEncoded);

    //     $this->persist($user);

    //     return $user;
    // }

    // protected function logIn(Client $client, string $email, string $password)
    // {
    //     $client->request('POST', '/auth/login', [
    //         'json' => [
    //             'email' => $email,
    //             'password' => $password,
    //         ],
    //     ]);
    //     $this->assertResponseStatusCodeSame(200);
    // }

    // protected function createUserAndLogIn(Client $client, string $email, string $password): User
    // {
    //     $user = $this->createUser($email, $password);
    //     $this->logIn($client, $email, $password);
    //     return $user;
    // }
}