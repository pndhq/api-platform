const API_URL = window.API_URL

if (!API_URL) {
  throw new Error('API_URL must be defined in window!')
}

class Api
{
  async request (url) {
    const res = await fetch(API_URL + url)
    console.log('[API]', API_URL, url, res)
    return res
  }
}

export default new Api()

export {
  Api,
}
