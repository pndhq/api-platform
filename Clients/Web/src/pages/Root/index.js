import React, { useEffect } from 'react'
import { Switch, Route } from 'react-router-dom'

import Welcome from 'pages/Welcome'

import api from 'lib/api'

export default () => {
  useEffect(() => {
    async function testApi () {
      return await api.request('/greetings')
    }

    testApi()
  }, [])

  return (
    <Switch>
      <Route exact path='/'>
        <Welcome />
      </Route>
    </Switch>
  )
}
