import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import 'normalize.css'

import Root from 'pages/Root'

import * as serviceWorker from './serviceWorker'

import 'styles/variables.css'
import 'styles/global.css'

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Root />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

serviceWorker.unregister()
