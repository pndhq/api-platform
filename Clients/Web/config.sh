if [ -z "$API_URL" ]; then
  source .env

  touch .env.local
  source .env.local
fi

echo "window.API_URL = '${API_URL}';" > ./public/config.js
