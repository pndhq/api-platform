.PHONY: images start stop shell

# Run the whole stack
start:
	@touch .env.local # in case it doesn't exist let's create it otherwise docker compose will scream
	@docker-compose up -d

# Stop the whole stack
stop:
	@docker-compose stop

# Open command line in API PHP
shell:
	@docker-compose exec api /bin/sh

# Build Docker images
images:
	@docker-compose build
