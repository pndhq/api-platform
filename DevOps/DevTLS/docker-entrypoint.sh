#!/bin/sh
set -ex

if [ -f /certs/localhost.crt ]; then
  echo "Found localhost.crt file, skipping..."
else
  mkcert --cert-file localhost.crt --key-file localhost.key localhost 127.0.0.1 ::1 mercure; \

  # the file must be named server.pem - the default certificate path in webpack-dev-server
  cat localhost.key localhost.crt > server.pem; \

  # export the root CA cert, but not the root CA key
  cp "$(mkcert -CAROOT)/rootCA.pem" /certs/localCA.crt
fi

exec "$@"
