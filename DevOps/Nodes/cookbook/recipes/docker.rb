include_recipe "chef-apt-docker"

docker_installation_package 'default' do
  action :create
  version '18.06.3'
end
