bash 'nameserver' do
  code <<-EOH
  systemctl mask systemd-resolved
  rm /etc/resolv.conf
  ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf
  EOH
end
