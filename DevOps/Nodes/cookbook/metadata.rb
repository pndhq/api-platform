name             "pndapiplatform"
maintainer       "Michal Palys-Dudek"
maintainer_email "michal@palys-dudek.pl"
description      "Generic cookbook for PND API Platform servers."
version          "1.0.0"

recipe           "pndapiplatform::docker", "Installs Docker Engine (CE)"
recipe           "pndapiplatform::nameserver", "Updates local caching nameserver to make Rancher Agent work"

depends "chef-apt-docker"
depends "docker"
