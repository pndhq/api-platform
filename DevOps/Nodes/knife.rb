cookbook_path    [".cookbooks"]
data_bag_path    "./databags"
environment_path "./environments"
role_path        "./roles"
node_path        "./"
#encrypted_data_bag_secret "data_bag_key"

knife[:berkshelf_path] = ".cookbooks"
