API Platform Nodes
==================

Chef provisioning recipes for raw nodes that will be hosting the project.

Applies some common configuration:

- ssh access via port `:3022` (lock down default port)
- ssh_key only access
- no root ssh access
- sets up users from `./databags/users` dir
- installs Docker engine

## Requirements

- ChefDK version 2.6.1 (https://downloads.chef.io/chefdk/stable/2.6.1)
- Knife Solo (https://matschaffer.github.io/knife-solo/) (be sure to install with `$ chef gem install knife-solo --pre`)

## Usage

  $ berks install
  $ chef exec knife solo bootstrap root@[ip] ./runlist.json

