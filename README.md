API Platform - PND Flavour
==========================

# Deploying

Some notes on deploying API platform to your servers.

## API PHP configuration

Set the correct ENV vars:

  TRUSTED_HOSTS=[your domain name]

## Vulcain

Vulcain needs to sit in front of API's Nginx container and route traffic there.
It also needs SSL to make use of HTTP/2, which means that you need to provide
your SSL certificates to the container (and configure `CERT_FILE` and `KEY_FILE`
env vars) and route HTTPS traffic to it:

- If your Load Balancer / Ingress supports SSL Passthrough then just use it.
- Otherwise Vulcain container needs to bind to host's `:443` port.

## Mercure

Mercure also needs to have access to SSL certs and have HTTPS traffic routed
to it so it can use features of HTTP/2. Therefore same issues apply as with
Vulcain above.
